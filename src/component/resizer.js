/* global window */
import { h } from './element';
import { mouseMoveUp } from './event';
import {cssPrefix, dprf} from '../config';

export default class Resizer {
  constructor(vertical = false, minDistance) {
    this.moving = false;
    this.vertical = vertical;
    this.el = h('div', `${cssPrefix}-resizer ${vertical ? 'vertical' : 'horizontal'}`).children(
      this.unhideHoverEl = h('div', `${cssPrefix}-resizer-hover`)
        .on('dblclick.stop', evt => this.mousedblclickHandler(evt))
        .css('position', 'absolute').hide(),
      this.hoverEl = h('div', `${cssPrefix}-resizer-hover`)
        .on('mousedown.stop', evt => this.mousedownHandler(evt)),
      this.lineEl = h('div', `${cssPrefix}-resizer-line`).hide(),
    ).hide();
    // cell rect
    this.cRect = null;
    this.finishedFn = null;
    this.minDistance = minDistance;
    this.unhideFn = () => {};
  }

  showUnhide(index) {
    this.unhideIndex = index;
    this.unhideHoverEl.show();
  }

  hideUnhide() {
    this.unhideHoverEl.hide();
  }

  // rect : {top, left, width, height}
  // line : {width, height}
  show(rect, line) {
    const {
      moving, vertical, hoverEl, lineEl, el,
      unhideHoverEl,
    } = this;
    if (moving) return;
    this.cRect = rect;
    const {
      left, top, width, height,
    } = rect;
    el.offset({
      left: vertical ? left + width - 5 : left,
      top: vertical ? top : top + height - 5,
    }).show();
    hoverEl.offset({
      width: vertical ? 5 : width,
      height: vertical ? height : 5,
    });
    lineEl.offset({
      width: vertical ? 0 : line.width,
      height: vertical ? line.height : 0,
    });
    unhideHoverEl.offset({
      left: vertical ? 5 - width : left,
      top: vertical ? top : 5 - height,
      width: vertical ? 5 : width,
      height: vertical ? height : 5,
    });
  }

  hide() {
    this.el.offset({
      left: 0,
      top: 0,
    }).hide();
    this.hideUnhide();
  }

  mousedblclickHandler() {
    if (this.unhideIndex) this.unhideFn(this.unhideIndex);
  }

  mousedownHandler(evt) {
    let startEvt = evt,startPos;
    const {
      el, lineEl, cRect, vertical, minDistance
    } = this;
    let distance , elSize;
    if(vertical){
      distance = cRect.width;
      elSize =  el.offset().width;
      startPos =  evt.screenX;
    }else{
      distance = cRect.height;
      elSize =  el.offset().height;
      startPos =  evt.screenY;
    }
    distance = distance - elSize;
    lineEl.show();
    mouseMoveUp(window, (e) => {
      this.moving = true;
      if (startEvt !== null && e.buttons === 1) {
        if (vertical) {
          let movementX;

          if(typeof e.movementX != 'undefined'){
            movementX = e.movementX;
          }else{
            movementX = e.screenX-startPos;
            startPos = e.screenX;
          }
          distance += movementX/dprf();
          if (distance > minDistance) {
            el.css('left', `${cRect.left + distance}px`);
          }
        } else {
          let movementY;
          if(typeof e.movementY != 'undefined'){
            movementY = e.movementY
          }else{
            movementY = e.screenY-startPos;
            startPos = e.screenY;
          }
          distance += movementY/dprf();
          if (distance > minDistance) {
            el.css('top', `${cRect.top + distance}px`);
          }
        }
        startEvt = e;
      }
    }, () => {
      startEvt = null;
      lineEl.hide();
      this.moving = false;
      this.hide();
      if (this.finishedFn) {
        distance = distance+elSize;
        if (distance < minDistance) distance = minDistance;
        this.finishedFn(cRect, distance);
      }
    });
  }
}
